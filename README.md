# cfs

> Crypto File Sharing

## Build Setup

Install MongoDB, Make sure its running.

``` bash
# install dependencies
npm install && npm install --prefix backend/

# serve with hot reload at localhost:8080
npm start
#On Windows, in one commmand prompt, 
npm start
#On another command prompt,
cd backend
npm start

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
