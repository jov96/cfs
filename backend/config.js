// config/database.js
module.exports = {

  'dbUrl': 'mongodb://localhost/cfs_005', // looks like mongodb://<user>:<pass>@mongo.onmodulus.net:27017/Mikha4ot
  'port': 4000, // Port to run our backend API server
  'destFolder': 'files/' // Location to upload encrypted files
}
