// load the things we need
const mongoose = require('mongoose')

// define the schema for our user module
const User = mongoose.Schema({
  name: String,
  username: { type: String, unique: true },
  receivedFiles: [{
    date: Date,
    from: String,
    message: String,
    filename: String,
    path: String,
    aesKey: String,
    eccKey: String
  }]
})

// create the model for users and expose it to our app
module.exports = mongoose.model('User', User)
