const User = require('./models/user')

module.exports = function (app, authServer, upload, fs, ecc) {
  /*
   * Route used when a new user is created. Stores his/her name and username on server.
   */
  app.post('/api/registerdetails', function (req, res) {
    var newUser = new User()
    newUser.name = req.body.name
    newUser.username = req.body.username
    newUser.save(function (err) {
      if (err) {
        console.log(err.message)
      } else {
        console.log('Account successfully created.')
        res.end('Success')
      }
    })
  })

  /*
   * Route used to fetch details of user and his/her mails when he/she logs in.
   */
  app.get('/api/getdetails', authServer.jwtProtector, function (req, res) {
    User.findOne({ username: req.user.username }).select('name receivedFiles').then(function (user) {
      if (user) {
        user.receivedFiles.forEach(element => {
          element.path = null
          element.aesKey = null
          element.eccKey = null
        })
        res.json(user)
      } else {
        console.log('Backend Error: ')
      }
    })
  })

  /*
   * Route used to check if given username is valid or not.
   */
  app.get('/api/validuser', authServer.jwtProtector, function (req, res) {
    User.findOne({ username: req.query.username }, function (err, user) {
      if (err) {
        console.log('Error')
      }
      if (!user) {
        res.end('false')
      } else {
        res.end('true')
      }
    })
  })

  /*
   * Route used to send mail to receiver. Adds enrty to receiver's received mails field
   * and uploads encrypted file to server.
   */
  app.post('/api/sendmail', authServer.jwtProtector, upload.single('file'), function (req, res) {
    User.findOne({ username: req.body.to }, function (err, user) {
      if (err) {
        res.end('Error')
      } else {
        if (!user) {
          res.sendStatus(400)
        } else {
          let mailIndex = user.receivedFiles.findIndex(function (element) {
            return element._id.equals(req.body.id)
          })
          if (mailIndex !== -1) {
            user.receivedFiles[mailIndex].date = Date.now()
            user.receivedFiles[mailIndex].from = req.user.username
            user.receivedFiles[mailIndex].message = req.body.message
            user.receivedFiles[mailIndex].filename = req.file.originalname
            user.receivedFiles[mailIndex].path = req.file.path
            user.receivedFiles[mailIndex].aesKey = req.body.encryptedAesKeys
            user.save()
            res.end('Mail Sent')
          } else {
            console.log('Couldnt find mail entry with given id.')
          }
        }
      }
    })
  })

  /*
   * Route used to delete a mail.
   */
  app.get('/api/deletemail', authServer.jwtProtector, function (req, res) {
    User.findOne({ username: req.user.username }, function (err, user) {
      if (err) {
        res.sendStatus(400)
      } else {
        let mailIndex = user.receivedFiles.findIndex(function (element) {
          return element._id.equals(req.query.id)
        })
        try { fs.unlinkSync(user.receivedFiles[mailIndex].path) } catch (e) { console.log(e) }
        user.receivedFiles.splice(mailIndex, 1)
        user.save()
        res.end('Success')
      }
    })
  })

  /*
   * Route used when user clicks the download button on a file It sents the encrypted file to the user.
   */
  app.get('/api/downloadfile', authServer.jwtProtector, function (req, res) {
    User.findOne({ username: req.user.username }, function (err, user) {
      if (err) {
        res.sendStatus(400)
      } else {
        var mailIndex = user.receivedFiles.findIndex(function (element) {
          return element._id.equals(req.query.id)
        })
        if (mailIndex !== -1) {
          res.download(user.receivedFiles[mailIndex].path)
        } else {
          res.sendStatus(400)
        }
      }
    })
  })

  /*
   * Route used to generate a new ECC private key/public key pair for sending mail.
   */
  app.get('/api/getpublickey', authServer.jwtProtector, function (req, res) {
    var eccKeys = ecc.generate(ecc.ENC_DEC, 384)
    var pubKey = eccKeys.enc
    var newMail = {
      eccKey: eccKeys.dec
    }
    User.findOneAndUpdate({ username: req.query.to }, { $push: { receivedFiles: newMail } }, function (err, data) {
      if (data) {
        User.findOne({ username: req.query.to }, function (err, data) {
          let newIndex = data.receivedFiles.findIndex(function (element) {
            return element.eccKey === eccKeys.dec
          })
          let id = data.receivedFiles[newIndex]._id
          res.json({ key: pubKey, id: id })
          if (err) {
            console.log('Error : ' + err)
          }
        })
      }
      if (err) {
        console.log('Error : ' + err)
      }
    })
  })

  /*
   * Route used to send the encrypted private ECC key for decrypting the
   * AES Key which is used for decrypting the file.
   */
  app.get('/api/getecckey', authServer.jwtProtector, function (req, res) {
    let ecc2PubKey = req.query.pubkey
    User.findOne({ username: req.user.username }, function (err, data) {
      if (data) {
        let newIndex = data.receivedFiles.findIndex(function (element) {
          return element._id.equals(req.query.id)
        })
        let eccPrivKey = data.receivedFiles[newIndex].eccKey
        let encryptedAesKey = data.receivedFiles[newIndex].aesKey
        var encryptedECCKey = ecc.encrypt(ecc2PubKey, eccPrivKey)

        res.json({ 'encryptedECCKey': encryptedECCKey, 'encryptedAesKey': encryptedAesKey })
        if (err) {
          console.log('Error : ' + err)
        }
      }
      if (err) {
        console.log('Error : ' + err)
      }
    })
  })

  /*
   *  Sample route.
   */
  app.get('/api/hello', function (req, res) {
    res.end('Hello')
  })

  /*
   *  Route used for regstering user, uses the vue-jwt-mongo module.
   */
  app.post('/auth/register', authServer.registerHandler)

  /*
   *  Route used for logging in user, uses the vue-jwt-mongo module.
   */
  app.post('/auth/login', authServer.loginHandler)

  /*
   *  Route used for refreshing login status, uses the vue-jwt-mongo module.
   */
  app.post('/auth/refresh', authServer.refreshHandler)
}
