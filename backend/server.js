// server.js

// set up ======================================================================
// get all the tools we need
console.log('Start of Server')
const express = require('express') // Importing Express.js
const app = express() // Express.js Initialization
const mongoose = require('mongoose') // Importing Mongoose.js
const morgan = require('morgan') // Import HTTP request logging tool
const bodyParser = require('body-parser') // Import tool to parse HTTP request body
const config = require('./config.js') // Import Database configuration
const multer = require('multer') // Multer for handling file uploads - multipart requests
const upload = multer({ dest: config.destFolder })
const fs = require('fs')
const ecc = require('eccjs')

// configuration ===============================================================
const port = config.port // Server Port

console.log('Creating database connection')
mongoose.connect(config.dbUrl) // connect to our database

// set up our express application
app.use(morgan('dev')) // log every request to the console
app.use(bodyParser.json()) // get information from html forms
app.use(bodyParser.urlencoded({
  extended: true
}))

const authServer = require('vue-jwt-mongo').Server({
  mongoUrl: config.dbUrl,
  jwtSecret: 'yolo123',
  userModelName: 'AuthUser'
})

console.log('Defining HTTP routes')
// routes ======================================================================
require('./routes.js')(app, authServer, upload, fs, ecc) // load our routes and pass in our app and fully configured passport
console.log('Starting Web server')
// launch ======================================================================
app.listen(port)
console.log('The magic happens on port ' + port)
